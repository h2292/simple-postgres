# simple-postgres

simple-postgres is a gradle project meant to illustrate a basic PostgreSQL backed Spring Boot application.  Some of what's illustrated:

* PostgreSQL database
* Hibernate ORM
* Automatic flyway database migration via spring boot
* Transactions and roll backs upon failure
* Caching via Ehcache

## Usage

Clone the repository and execute the following. This will start the app and the database. Spring boot will automatically apply the flyway scripts.

```
./gradlew install
```

Hit some of the application endpoints by importing the Postman collection in the `postman-collection.json` file.

## License
[MIT](https://choosealicense.com/licenses/mit/)